package com.example.examen;

import java.io.Serializable;

public class Rectangulo implements Serializable {

    private float base;
    private float altura;

    public Rectangulo(float base, float altura)
    {
        this.setBase(base);
        this.setAltura(altura);
    }
    public Rectangulo()
    {

    }

    public float getBase() {
        return base;
    }

    public void setBase(float base) {
        this.base = base;
    }

    public float getAltura() {
        return altura;
    }

    public void setAltura(float altura) {
        this.altura = altura;
    }
    public float CalcularArea()
    {
        float area = this.base * this.altura;
        return area;
    }
    public float CalcularPerimetro()
    {
       float perimetro = (this.base * 2) + (this.altura * 2);
       return perimetro;
    }
}
