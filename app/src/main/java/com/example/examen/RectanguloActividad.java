package com.example.examen;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class RectanguloActividad extends AppCompatActivity {
    private TextView lblCliente;
    private EditText txtBase;
    private EditText txtAltura;
    private TextView lblArea;
    private TextView lblPerimetro;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;
    private Rectangulo rectangulo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rectangulo_actividad);

        lblCliente = (TextView) findViewById(R.id.lblCliente);
        txtBase = (EditText) findViewById(R.id.txtBase);
        txtAltura = (EditText) findViewById(R.id.txtAltura);
        lblArea = (TextView) findViewById(R.id.lblArea);
        lblPerimetro = (TextView) findViewById(R.id.lblPerimetro);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);
        rectangulo = new Rectangulo();

        Bundle datos = getIntent().getExtras();
        String cliente = datos.getString("cliente");
        lblCliente.setText("Mi nombre es: " + cliente);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtBase.getText().toString().matches(""))
                {
                    Toast.makeText(RectanguloActividad.this, "Existen campos vacíos", Toast.LENGTH_SHORT).show();
                }
                else if(txtAltura.getText().toString().matches(""))
                {
                    Toast.makeText(RectanguloActividad.this, "Existen Campos Vacíos", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    String bas = txtBase.getText().toString();
                    String alt = txtAltura.getText().toString();

                    float base = Float.parseFloat(bas);
                    float altura = Float.parseFloat(alt);

                    rectangulo.setBase(base);
                    rectangulo.setAltura(altura);

                    String area = String.valueOf(rectangulo.CalcularArea());
                    String perimetro = String.valueOf(rectangulo.CalcularPerimetro());

                    lblArea.setText("Area: " + area);
                    lblPerimetro.setText("Perimetro: " + perimetro);
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtBase.setText("");
                txtAltura.setText("");
                lblArea.setText("");
                lblPerimetro.setText("");
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
